Installation instructions for katello and foreman on centos7


hostnamectl set-hostname katello.example.com
yum update -y
yum install -y epel-release
yum install -y  https://yum.theforeman.org/releases/1.22/el7/x86_64/foreman-release.rpm
yum install -y https://fedorapeople.org/groups/katello/releases/yum/3.12/katello/el7/x86_64/katello-repos-latest.rpm
yum install -y foreman-release-scl
yum install -y katello
foreman-installer --scenario katello --foreman-initial-admin-password katello --disable-system-checks

